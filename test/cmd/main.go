package main

import (
	godirectus "go-directus"
)

type Course struct {
	ID            string   `json:"id"`
	Header        string   `json:"header"`
	MetaEnabled   bool     `json:"meta_enabled"`
	MetaImage     string   `json:"meta_image"`
	Level         string   `json:"level"`
	Volume        int      `json:"volume"`
	Duration      int      `json:"duration"`
	NearlestStart string   `json:"nearlest_start"`
	Period        string   `json:"period"`
	EduType       string   `json:"edu_type"`
	EduLanguage   string   `json:"edu_language"`
	EduTime       string   `json:"edu_time"`
	Document      string   `json:"document"`
	Cost          int      `json:"cost"`
	OriginalCost  int      `json:"original_cost"`
	Title         string   `json:"title"`
	Description   string   `json:"description"`
	Program       string   `json:"program"`
	Developers    []int    `json:"developers"`
	Categories    []int    `json:"categories"`
	Modules       []string `json:"modules"`
}

func main() {

	dirclient, err := godirectus.NewClient("https://api.do.kosygin-rsu.ru")
	if err != nil {
		panic(err)
	}
	// Object type, primary id type
	coursesCollection := godirectus.GetCollection[Course, string](dirclient, "courses")
	c, err := coursesCollection.ReadOne("03e6810e-8ebe-418e-8933-c1db3da81905")
	if err != nil {
		panic(err)
	}
	_ = c
}
