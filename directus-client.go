package godirectus

import (
	"fmt"
	"io"
	"net/http"
)

type DirectusApiClient struct {
	baseAddr string
	client   http.Client
}

func NewClient(addr string) (*DirectusApiClient, error) {
	h := &DirectusApiClient{
		baseAddr: addr,
		client:   http.Client{},
	}
	if err := h.Ping(); err != nil {
		return nil, err
	}
	return h, nil
}

// /server/ping
func (h *DirectusApiClient) Ping() error {
	url := combinePath(h.baseAddr, "server/ping")
	req, err := buildRequest(url, "GET", nil, nil)
	if err != nil {
		return err
	}
	response, err := h.client.Do(req)
	if err != nil {
		return err
	}
	if response.StatusCode != 200 {
		return fmt.Errorf("ping failed")
	}
	return nil
}

func GetCollection[T interface{}, IDType directusIdType](client *DirectusApiClient, collection string) *DirectusCollectionAccessor[T, IDType] {
	addr := combinePath(client.baseAddr, "items", collection)
	return &DirectusCollectionAccessor[T, IDType]{
		collectionAddr: addr,
		client:         &client.client,
	}
}

func combinePath(path ...string) string {
	var r string = ""
	for idx, v := range path {
		if v[0] == '/' {
			path = path[1:]
		}
		if v[len(v)-1] == '/' {
			v = v[:len(v)-1]
		}
		if idx == 0 {
			r = v
		} else {
			r = fmt.Sprintf("%s/%s", r, v)
		}
	}
	return r
}

func buildRequest(path, method string, query map[string]string, body io.Reader) (*http.Request, error) {

	req, err := http.NewRequest("GET", path, nil)
	if err != nil {
		return nil, err
	}
	if query != nil {
		q := req.URL.Query()
		for k, v := range query {
			q.Add(k, v)
		}
		req.URL.RawQuery = q.Encode()
	}
	return req, nil
}
