package godirectus

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/google/uuid"
)

type directusIdType interface {
	string | uuid.UUID | uint64
}

func directusId2String[IDType directusIdType](id IDType) string {
	switch any(id).(type) {
	case string:
		return any(id).(string)
	case uuid.UUID:
		return any(id).(uuid.UUID).String()
	case uint64:
		return strconv.FormatUint(any(id).(uint64), 64)
	}
	panic("HOW DID YOU GET HERE??????????????")
}

type directusManyResponse[T interface{}] struct {
	Errors []directusError `json:"errors"`
	Data   []T             `json:"data"`
}
type directusSingleResponse[T interface{}] struct {
	Errors []directusError `json:"errors"`
	Data   *T              `json:"data"`
}
type directusError struct {
	Message    string `json:"message"`
	Extensions struct {
		Code string `json:"code"`
	} `json:"extensions"`
}

type DirectusCollectionAccessor[T interface{}, IDType directusIdType] struct {
	collectionAddr string
	client         *http.Client
}

func (h *DirectusCollectionAccessor[T, IDType]) ReadOne(id IDType) (*directusSingleResponse[T], error) {
	//GET
	url := combinePath(h.collectionAddr, directusId2String(id))
	request, err := buildRequest(url, "GET", nil, nil)
	if err != nil {
		return nil, err
	}
	resp, err := h.client.Do(request)
	if err != nil {
		return nil, err
	}
	result := &directusSingleResponse[T]{}
	err = json.NewDecoder(resp.Body).Decode(result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

func (h *DirectusCollectionAccessor[T, IDType]) ReadMany() {

}

func (h *DirectusCollectionAccessor[T, IDType]) ReadByQuery() (*directusManyResponse[T], error) {
	//GET
	request, err := buildRequest(h.collectionAddr, "GET", nil, nil)
	if err != nil {
		return nil, err
	}
	resp, err := h.client.Do(request)
	if err != nil {
		return nil, err
	}
	result := &directusManyResponse[T]{}
	err = json.NewDecoder(resp.Body).Decode(result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

/*
func (h *DirectusCollectionAccessor[T]) CreateOne() {

}

func (h *DirectusCollectionAccessor[T]) CreateMany() {

}

func (h *DirectusCollectionAccessor[T]) UpdateOne() {

}

func (h *DirectusCollectionAccessor[T]) UpdateMany() {

}

func (h *DirectusCollectionAccessor[T]) DeleteOne() {

}

func (h *DirectusCollectionAccessor[T]) DeleteMany() {

}
*/
