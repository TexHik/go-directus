package godirectus

import "github.com/google/uuid"

type DirectusCollection[T interface{}] struct {
	Id uuid.UUID `json:"id"`
}
